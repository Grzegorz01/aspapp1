﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASPApp1.Startup))]
namespace ASPApp1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
